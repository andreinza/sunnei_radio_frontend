
import { createRouter , createWebHistory } from 'vue-router'
// import Home from '@/views/Home.vue'


const routes =[
    
    // {
    //     path:'/', name:'Home' , component:Home
    // },
    {
        path:'/', name:'schedule' , component: ()=>import('@/views/Schedule.vue')
    },
    {
        path:'/archive', name:'archive' , component: ()=>import('@/views/Archive.vue')
    }
    
]


const router = createRouter({
    history:createWebHistory(),
    routes,
    linkActiveClass:'cambiacolore'
})

export default router
